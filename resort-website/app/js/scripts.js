$(document).ready(function(){
    $(".two").click(function(){
        $(".two-content").css({
           'display': 'block' ,
            'transition': '0.4s'
        });
        $(".one-content").css({
           'display': 'none' 
        });
         $(".three-content").css({
           'display': 'none' 
        });
        $(".one").css({
           'background': '#ccc'
        });
        $(".three").css({
           'background': '#ccc'
        });
        $(".two").css({
           'background': '#B0914F'
        });
         $(".two-content").addClass("animated");
        $(".two-content").addClass("bounceInRight");
    });
    
     $(".three").click(function(){
        $(".three-content").css({
           'display': 'block',
            'transition': '0.4s'
        });
        $(".one-content").css({
           'display': 'none' 
        });
         $(".two-content").css({
           'display': 'none' 
        });
        $(".one").css({
           'background': '#ccc'
        });
        $(".two").css({
           'background': '#ccc'
        });
        $(".three").css({
           'background': '#B0914F'
        });
          $(".three-content").addClass("animated");
        $(".three-content").addClass("bounceInRight");
    });
    
    $(".one").click(function(){
        $(".one-content").css({
           'display': 'block',
            'transition': '0.4s'
        });
        $(".two-content").css({
           'display': 'none' 
        });
         $(".three-content").css({
           'display': 'none' 
        });
        $(".two").css({
           'background': '#ccc'
        });
        $(".three").css({
           'background': '#ccc'
        });
        $(".one").css({
           'background': '#B0914F'
        });
          $(".one-content").addClass("animated");
        $(".one-content").addClass("bounceInLeft");
    });
    
    $(".header__menu-btn").click(function(){
        $(".header").css({
           'display': 'none' 
        });
        $(".app-center").css({
           'display': 'none'
        });
        $(".small-menu-page").css({
           'display': 'block'
        });
    });
    
     $(".small-menu-page__cross").click(function(){
         $(".header__menu-btn").css({
           'display': 'block' 
        });
         $(".small-menu-page").css({
           'display': 'none' 
        });
         $(".header").css({
           'display': 'block' 
        });
        $(".app-center").css({
           'display': 'block'
        });
     });
    
    window.onscroll = function(){
        if(document.body.scrollTop>50 || document.documentElement.scrollTop>50){
            $(".up-button").css({
                'display': 'block'
            });
        }else{
            $(".up-button").css({
                'display': 'none'
            });
        }
    }
    
    $(".up-button").click(function(){
       document.documentElement.scrollTop = 0; 
        setInterval(function(){
            
        }, 3000);
    });
    
    /************************************************************************
                        GOOGLE MAPS
************************************************************************/

$(window).on('load', function(){
   var addressString = "302, Evergreen CHS,<br>Airoli, Maharashtra, <br>India";
    var myLatLng = {
      lat: 19.173829,
      lng: 72.953716
    };
    var myMap = new google.maps.Map(document.getElementById('map'),{
       zoom: 17,
        center: myLatLng
    });
    
    var marker = new google.maps.Marker({
       position: myLatLng,
        map: myMap,
        title: "Click to see address!"
    });
    
    var infoWindow = new google.maps.InfoWindow({
       content: addressString 
    });
    marker.addListener('click', function(){
        infoWindow.open(myMap, marker);
    })  
});
    
});
